
This download may not be the latest version!

The latest version of the Universal Library for LabVIEW, for Win2000/XP/Vista, can be downloaded 
for free with all of MCCDAQ software here: ftp://ftp.measurementcomputing.com/downloads/MCCDaqCD/

***********************************************************************************************

Universal Library for LabVIEW - Version 7.11a

This media provides support for LabVIEW version 6 or later on Windows(R) 9.x/2000/XP.
Version 7.11a differs from version 7.11 only in that it can be installed on LabVIEW
version 8.

The program "ULforLV711a.exe" is a self-extracting ZIP file that contains the CD image.

To extract the CD image, simply double click on the file.  Follow the on-screen
instructions, including entering the characters of the password into the password
dialog box.  This will unzip the files into your system TEMP directory if you do not
choose a directory to extract to.

The password is:  mcclv711a

See the installation readme.txt file (after unzipping) for complete revision information.