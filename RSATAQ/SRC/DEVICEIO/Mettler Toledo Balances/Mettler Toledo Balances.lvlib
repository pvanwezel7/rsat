﻿<?xml version='1.0' encoding='UTF-8'?>
<Library LVVersion="17008000">
	<Property Name="Instrument Driver" Type="Str">True</Property>
	<Property Name="NI.Lib.DefaultMenu" Type="Str">dir.mnu</Property>
	<Property Name="NI.Lib.Description" Type="Str">This driver will configure and take measurements from Mettler Toledo balances.  For additional information see Mettler Toledo Balances Readme.html</Property>
	<Property Name="NI.Lib.HelpPath" Type="Str"></Property>
	<Property Name="NI.Lib.Icon" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!)_!!!*Q(C=\&gt;4.=?*!%)&lt;B4SY@@&amp;RN"#Z3["1Y_U9+H1*8_1;V%81)3Q3O)I6/Q!&gt;3)!8NK[("^NI,&amp;V0FL&lt;,'!_+&lt;PU=D76)\\K2&lt;D:?/B\0(K8VM&gt;?L`:MR,_^D/R_L[Q@CJ(.P(9[?`ZD_.(^_0@TA\`J^_H4W_W\`&lt;`_`W#Q&gt;^BIO&gt;LHR)8:.3ERJ5JVK\OD(*ETT*ETT*ETT)ATT)ATT)A^T*H&gt;T*H&gt;T*H&gt;T)D&gt;T)D&gt;T)D&lt;Q6=J',8/4M3L*YMF!S;4*"UBG+EI`%EXA34_,BJR*0YEE]C3@RU%7**`%EHM34?"CGR*.Y%E`C34R-V34:#DG?R-0U#DS"*`!%HM$$EAI]!3"9,*AYG!3'AM&lt;A*0!%HM$$K1*0Y!E]A3@QU+T!%XA#4_!*0!RJOR*.-R6S0%QDR_.Y()`D=4R-,=@D?"S0YX%],#@(YXA=B,/A-TE%/9/=$MY0R_.Y_*,D=4S/R`%Y(JL;(@+W-Z.G+O2Y$)`B-4S'R`!QB1S0Y4%]BM@Q-+U-D_%R0)&lt;(],#5$)`B-4Q'R&amp;C5Z76-:AQU/BG"Y?'P03X7\F)UC&lt;63`ZK("V8V!+I?,.5$IXI16$&gt;9&gt;?.5.U3VU;I.6'W-[I*6&amp;[)#KB:74;DKK$W@/_K7OK'OK%PKADKHTKD^.034/_\X?_VW/WWX7WUW'[V7+SW83SU7#]XH=]VG-`6^@XJ&lt;X6-/2X&gt;],QW=$\_?OO(Z2T@]`PEYL0NO70-^Z6?MB`@36XAX[E&lt;DK^=]?`1(B;"3KQ!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">385908736</Property>
	<Property Name="NI.Lib.Version" Type="Str">2.0.0.0</Property>
	<Property Name="NI.SortType" Type="Int">3</Property>
	<Item Name="Public" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
		<Item Name="Action-Status" Type="Folder">
			<Item Name="Action-Status.mnu" Type="Document" URL="../Public/Action-Status/Action-Status.mnu"/>
			<Item Name="Print Text.vi" Type="VI" URL="../Public/Action-Status/Print Text.vi"/>
			<Item Name="Print Weight.vi" Type="VI" URL="../Public/Action-Status/Print Weight.vi"/>
		</Item>
		<Item Name="Configure" Type="Folder">
			<Item Name="Advanced Parameters" Type="Folder">
				<Item Name="Configure_Advanced Parameters.mnu" Type="Document" URL="../Public/Configure/Advanced Parameters/Configure_Advanced Parameters.mnu"/>
				<Item Name="Advanced Parameters.vi" Type="VI" URL="../Public/Configure/Advanced Parameters/Advanced Parameters.vi"/>
				<Item Name="Display.vi" Type="VI" URL="../Public/Configure/Advanced Parameters/Display.vi"/>
				<Item Name="Power.vi" Type="VI" URL="../Public/Configure/Advanced Parameters/Power.vi"/>
				<Item Name="Set Language.vi" Type="VI" URL="../Public/Configure/Advanced Parameters/Set Language.vi"/>
				<Item Name="Unit.vi" Type="VI" URL="../Public/Configure/Advanced Parameters/Unit.vi"/>
			</Item>
			<Item Name="Applications" Type="Folder">
				<Item Name="Configure_Applications.mnu" Type="Document" URL="../Public/Configure/Applications/Configure_Applications.mnu"/>
				<Item Name="Application Selection.vi" Type="VI" URL="../Public/Configure/Applications/Application Selection.vi"/>
				<Item Name="Piece Weight.vi" Type="VI" URL="../Public/Configure/Applications/Piece Weight.vi"/>
				<Item Name="Reference.vi" Type="VI" URL="../Public/Configure/Applications/Reference.vi"/>
			</Item>
			<Item Name="Balance Settings" Type="Folder">
				<Item Name="Configure_Balance Settings.mnu" Type="Document" URL="../Public/Configure/Balance Settings/Configure_Balance Settings.mnu"/>
				<Item Name="Balance Mode.vi" Type="VI" URL="../Public/Configure/Balance Settings/Balance Mode.vi"/>
				<Item Name="Door Manipulation.vi" Type="VI" URL="../Public/Configure/Balance Settings/Door Manipulation.vi"/>
				<Item Name="Power On Mode.vi" Type="VI" URL="../Public/Configure/Balance Settings/Power On Mode.vi"/>
			</Item>
			<Item Name="Beeper" Type="Folder">
				<Item Name="Configure_Beeper.mnu" Type="Document" URL="../Public/Configure/Beeper/Configure_Beeper.mnu"/>
				<Item Name="Beep.vi" Type="VI" URL="../Public/Configure/Beeper/Beep.vi"/>
			</Item>
			<Item Name="Date - Time" Type="Folder">
				<Item Name="Configure_Date - Time.mnu" Type="Document" URL="../Public/Configure/Date - Time/Configure_Date - Time.mnu"/>
				<Item Name="Date.vi" Type="VI" URL="../Public/Configure/Date - Time/Date.vi"/>
				<Item Name="Time.vi" Type="VI" URL="../Public/Configure/Date - Time/Time.vi"/>
			</Item>
			<Item Name="Tare-Zero-Calibration" Type="Folder">
				<Item Name="Configure_Tare-Zero-Calibration.mnu" Type="Document" URL="../Public/Configure/Tare-Zero-Calibration/Configure_Tare-Zero-Calibration.mnu"/>
				<Item Name="Internal Calibration.vi" Type="VI" URL="../Public/Configure/Tare-Zero-Calibration/Internal Calibration.vi"/>
				<Item Name="Internal Test.vi" Type="VI" URL="../Public/Configure/Tare-Zero-Calibration/Internal Test.vi"/>
				<Item Name="ProFACT.vi" Type="VI" URL="../Public/Configure/Tare-Zero-Calibration/ProFACT.vi"/>
				<Item Name="Tare.vi" Type="VI" URL="../Public/Configure/Tare-Zero-Calibration/Tare.vi"/>
				<Item Name="Test And Adjustment Weight.vi" Type="VI" URL="../Public/Configure/Tare-Zero-Calibration/Test And Adjustment Weight.vi"/>
				<Item Name="Zero.vi" Type="VI" URL="../Public/Configure/Tare-Zero-Calibration/Zero.vi"/>
			</Item>
			<Item Name="Weighing Parameters" Type="Folder">
				<Item Name="Configure_Weighing Parameters.mnu" Type="Document" URL="../Public/Configure/Weighing Parameters/Configure_Weighing Parameters.mnu"/>
				<Item Name="Readability.vi" Type="VI" URL="../Public/Configure/Weighing Parameters/Readability.vi"/>
				<Item Name="Weighing Parameters.vi" Type="VI" URL="../Public/Configure/Weighing Parameters/Weighing Parameters.vi"/>
			</Item>
			<Item Name="Configure.mnu" Type="Document" URL="../Public/Configure/Configure.mnu"/>
		</Item>
		<Item Name="Data" Type="Folder">
			<Item Name="Data.mnu" Type="Document" URL="../Public/Data/Data.mnu"/>
			<Item Name="Get Weight Value With Units.vi" Type="VI" URL="../Public/Data/Get Weight Value With Units.vi"/>
			<Item Name="Get Weight Value.vi" Type="VI" URL="../Public/Data/Get Weight Value.vi"/>
			<Item Name="Temperature.vi" Type="VI" URL="../Public/Data/Temperature.vi"/>
		</Item>
		<Item Name="Utility" Type="Folder">
			<Item Name="Utility.mnu" Type="Document" URL="../Public/Utility/Utility.mnu"/>
			<Item Name="Balance Type.vi" Type="VI" URL="../Public/Utility/Balance Type.vi"/>
			<Item Name="Error Message.vi" Type="VI" URL="../Public/Utility/Error Message.vi"/>
			<Item Name="Reset.vi" Type="VI" URL="../Public/Utility/Reset.vi"/>
			<Item Name="Revision Query.vi" Type="VI" URL="../Public/Utility/Revision Query.vi"/>
			<Item Name="Update Rate.vi" Type="VI" URL="../Public/Utility/Update Rate.vi"/>
		</Item>
		<Item Name="dir.mnu" Type="Document" URL="../Public/dir.mnu"/>
		<Item Name="Close.vi" Type="VI" URL="../Public/Close.vi"/>
		<Item Name="Initialize.vi" Type="VI" URL="../Public/Initialize.vi"/>
		<Item Name="VI Tree.vi" Type="VI" URL="../Public/VI Tree.vi"/>
	</Item>
	<Item Name="Private" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
		<Item Name="Command.vi" Type="VI" URL="../Private/Command.vi"/>
		<Item Name="Read.vi" Type="VI" URL="../Private/Read.vi"/>
	</Item>
</Library>
