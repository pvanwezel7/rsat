------------------------------------------------------------------------
Universal Library(TM) for LabVIEW(TM)
------------------------------------------------------------

LabVIEW Interface
------------------------------------------------------------
Thank you for purchasing the Universal Library(TM) for LabVIEW(TM). 
This media provides support for LabVIEW v6 or later on Windows(R) 2000/XP/Vista.
For support, please call us at (508) 946 5100.

New in this release
------------------------------------------------------------
Two new examples added to suport the new released USB-TEMP and USB-TC hardware

XTIN.VI     	Single temperature input in a while loop with a metered display. Uses TIn VI.
XTINSCAN.VI 	Temperature input scan across multiple channels in a while loop.
		Data is displayed on a strip-chart. Uses TInScan VI.

Traditional NI DAQ support
------------------------------------------------------------
Using MCC hardware with the Traditional NI DAQ VIs is no longer supported in this release.
You may use the MCC UL Extension VIs to obtain full functionality for your hardware.


Installation
------------------------------------------------------------
Execute the setup program for the Univesal Library(TM) for LabVIEW(TM)
and follow the installation instructions given on the screen.

The following examples are provided to demonstrate the LabVIEW(TM) VI interface;

Example VI	Description
----------	------------------------------------------------------------------
XAIN.VI     	Single analog input in a while loop with a metered display.
XAINSCBG.VI 	Analog input scan in the background. Uses GetStat, StopBg, and
		OptAin VI's. The data is displayed on a graph.
XAINSCFG.VI 	Analog input scan in the foreground. Uses SelChan, and OptAin VI's.
		Data is displayed on a graph. 
XAICNBG.VI	New example that demonstrates real-time data display while 
		performing a continuous background scan. (32-bit only)
XAIOSCBG.VI	Demonstrates concurrent analog input and output scans.
XAOUT.VI    	Single analog output. Demonstrates sequences, case statements, 
		for loops, and while loops.
XAOUTSCB.VI 	Analog output scan in the background. Uses GetStat and StopBg VI's.
XAOUTSCF.VI 	Analog output scan in the foreground. Generates sinusoidal data. 
XAPRETRB.VI 	Analog pre-trigger in the background. Uses GetStat, StopBg and 
		ACnvPrDt VI's, output is displayed on a graph.
XAPRETRF.VI 	Analog pre-trigger in the foreground. Uses SelChan and ACnvPrDt
		VI's. Displays data on a graph.
XASCFILE.VI 	Analog input to a file. Uses FileAInS and FileRead. Display's data
		on a graph. 
XASCMEM.VI  	Analog input to a memory board. Uses MemReset and MemRead VI's. 
		Data is displayed on a graph. 
XCFREQ.VI   	Displays the frequency of a signal at a counter input. Uses 
		C9513Ini and CFreqIn VI's. 
XCSTORE.VI  	Stores counter values when an interrupt occurs. Uses CStore, 
		GetStat, and StopBg.
XCTR7266.VI	Configures, loads, reads and displays an 7266 quadrature counter.
		Uses C7266Cfg, Cload32, Cstatus and Cin32 VI's.
XCTR8254.VI 	Configures, loads, reads and displays an 8254 counter. Uses
		C8254Cfg, CLoad and CIn VI's. 
XCTR8536.VI 	Configures, loads, reads and displays an 8536 counter. Uses
		C8536Cfg, CLoad and CIn VI's. 
XCTR9513.VI 	Configures, loads, reads and displays a 9513 counter. Uses
		C9513Cfg, CLoad and CIn VI's. 
XDBITIN.VI  	Configures and reads a digital bit. Toggles a display LED 
		according to the state of the bit. Uses DCfgPort and DBitIn.
XDBITOUT.VI 	Configures and writes a digital bit. Uses DCfgPort and DBitOut. 
XDCFGBIT.VI	Configures bitwise configurable digital port.
XDIN.VI     	Configures and reads a digital port. Toggles 8 display LED's 
		accordingly. Uses DCfgPort and DIn VI's. 
XDINSCBG.VI 	Performs a continuous background read of the specified digital 
		port. Uses DCfgPort, DInScBg, GetStat, and StopBg VI's.	
XDINSCFG.VI 	Performs a continuous foreground read of the specified digital 
		port. Uses DCfgPort, and DInScFg VI's. 
XDOUT.VI    	Configures and writes a digital port. Uses DCfgPort and DOut VI's.
XDOUTSCB.VI 	Writes multiple bytes to a specified digital port in the background.
		Uses DCfgPort, DOutScBg, GetStat, and StopBg VI's. 
XDOUTSCF.VI     Writes multiple bytes to a specified digital port in the foreground.
		Uses DCfgPort and DOutScFg VI's. 
XEVENTCTR.VI 	Resets, reads and displays an event counter. Uses CLoad32 and CIn32 VI's.
		To use in conjunction with PMD / USB series.
XTIN.VI     	Single temperature input in a while loop with a metered display. Uses TIn VI.
XTINSCAN.VI 	Temperature input scan across multiple channels in a while loop.
		Data is displayed on a strip-chart. Uses TInScan VI.

Note: Not all of the examples are supported on all hardware platforms. Please refer 
to the User Manual to confirm what LabVIEW VI's are supported with your particular
hardware.

Each of the examples are built on top of a set of VIs that are contained in the
library DAS16.LLB. Each of the VIs contained in the library are documented in the 
User Manual. For a detailed overview of the available functions please refer to 
the manual. 

Universal Library(TM) is a trademark of Measurement Computing Corp.
LabVIEW(TM) is a trademark of National Instruments Corp.
Windows(R) is a registered trademark of Microsoft Corp.