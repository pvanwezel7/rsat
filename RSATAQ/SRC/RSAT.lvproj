﻿<?xml version='1.0' encoding='UTF-8'?>
<Project Type="Project" LVVersion="17008000">
	<Item Name="My Computer" Type="My Computer">
		<Property Name="IOScan.Faults" Type="Str"></Property>
		<Property Name="IOScan.NetVarPeriod" Type="UInt">100</Property>
		<Property Name="IOScan.NetWatchdogEnabled" Type="Bool">false</Property>
		<Property Name="IOScan.Period" Type="UInt">10000</Property>
		<Property Name="IOScan.PowerupMode" Type="UInt">0</Property>
		<Property Name="IOScan.Priority" Type="UInt">9</Property>
		<Property Name="IOScan.ReportModeConflict" Type="Bool">true</Property>
		<Property Name="IOScan.StartEngineOnDeploy" Type="Bool">false</Property>
		<Property Name="server.app.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.control.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.tcp.enabled" Type="Bool">false</Property>
		<Property Name="server.tcp.port" Type="Int">0</Property>
		<Property Name="server.tcp.serviceName" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.tcp.serviceName.default" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.vi.callsEnabled" Type="Bool">true</Property>
		<Property Name="server.vi.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="specify.custom.address" Type="Bool">false</Property>
		<Item Name="SRC" Type="Folder">
			<Item Name="DEVICEIO" Type="Folder">
				<Item Name="Meilhaus" Type="Folder">
					<Item Name="SensorCalibration" Type="Folder"/>
					<Item Name="Universal Library" Type="Folder">
						<Item Name="Das16.llb" Type="Folder">
							<Item Name="ACalData.VI" Type="VI" URL="../DEVICEIO/Meilhaus/Universal Library/Das16.llb/ACalData.VI"/>
							<Item Name="ACNVPRDT.VI" Type="VI" URL="../DEVICEIO/Meilhaus/Universal Library/Das16.llb/ACNVPRDT.VI"/>
							<Item Name="ACvtData.VI" Type="VI" URL="../DEVICEIO/Meilhaus/Universal Library/Das16.llb/ACvtData.VI"/>
							<Item Name="AIn.VI" Type="VI" URL="../DEVICEIO/Meilhaus/Universal Library/Das16.llb/AIn.VI"/>
							<Item Name="AINSCBG.VI" Type="VI" URL="../DEVICEIO/Meilhaus/Universal Library/Das16.llb/AINSCBG.VI"/>
							<Item Name="AInScFg.VI" Type="VI" URL="../DEVICEIO/Meilhaus/Universal Library/Das16.llb/AInScFg.VI"/>
							<Item Name="ALoadQue.VI" Type="VI" URL="../DEVICEIO/Meilhaus/Universal Library/Das16.llb/ALoadQue.VI"/>
							<Item Name="AOut.VI" Type="VI" URL="../DEVICEIO/Meilhaus/Universal Library/Das16.llb/AOut.VI"/>
							<Item Name="AOutScBg.VI" Type="VI" URL="../DEVICEIO/Meilhaus/Universal Library/Das16.llb/AOutScBg.VI"/>
							<Item Name="AOutScFg.VI" Type="VI" URL="../DEVICEIO/Meilhaus/Universal Library/Das16.llb/AOutScFg.VI"/>
							<Item Name="APretrBg.VI" Type="VI" URL="../DEVICEIO/Meilhaus/Universal Library/Das16.llb/APretrBg.VI"/>
							<Item Name="APretrFg.VI" Type="VI" URL="../DEVICEIO/Meilhaus/Universal Library/Das16.llb/APretrFg.VI"/>
							<Item Name="ATrig.VI" Type="VI" URL="../DEVICEIO/Meilhaus/Universal Library/Das16.llb/ATrig.VI"/>
							<Item Name="C7266Config.VI" Type="VI" URL="../DEVICEIO/Meilhaus/Universal Library/Das16.llb/C7266Config.VI"/>
							<Item Name="C8254Cfg.VI" Type="VI" URL="../DEVICEIO/Meilhaus/Universal Library/Das16.llb/C8254Cfg.VI"/>
							<Item Name="C8536CFG.VI" Type="VI" URL="../DEVICEIO/Meilhaus/Universal Library/Das16.llb/C8536CFG.VI"/>
							<Item Name="C8536INI.VI" Type="VI" URL="../DEVICEIO/Meilhaus/Universal Library/Das16.llb/C8536INI.VI"/>
							<Item Name="C9513CFG.VI" Type="VI" URL="../DEVICEIO/Meilhaus/Universal Library/Das16.llb/C9513CFG.VI"/>
							<Item Name="C9513INI.VI" Type="VI" URL="../DEVICEIO/Meilhaus/Universal Library/Das16.llb/C9513INI.VI"/>
							<Item Name="CFreqIn.VI" Type="VI" URL="../DEVICEIO/Meilhaus/Universal Library/Das16.llb/CFreqIn.VI"/>
							<Item Name="CIn.vi" Type="VI" URL="../DEVICEIO/Meilhaus/Universal Library/Das16.llb/CIn.vi"/>
							<Item Name="CIN32.VI" Type="VI" URL="../DEVICEIO/Meilhaus/Universal Library/Das16.llb/CIN32.VI"/>
							<Item Name="CLOAD.VI" Type="VI" URL="../DEVICEIO/Meilhaus/Universal Library/Das16.llb/CLOAD.VI"/>
							<Item Name="CLOAD32.VI" Type="VI" URL="../DEVICEIO/Meilhaus/Universal Library/Das16.llb/CLOAD32.VI"/>
							<Item Name="CStatus.VI" Type="VI" URL="../DEVICEIO/Meilhaus/Universal Library/Das16.llb/CStatus.VI"/>
							<Item Name="CSTORE.VI" Type="VI" URL="../DEVICEIO/Meilhaus/Universal Library/Das16.llb/CSTORE.VI"/>
							<Item Name="DBitIn.VI" Type="VI" URL="../DEVICEIO/Meilhaus/Universal Library/Das16.llb/DBitIn.VI"/>
							<Item Name="DBitOut.VI" Type="VI" URL="../DEVICEIO/Meilhaus/Universal Library/Das16.llb/DBitOut.VI"/>
							<Item Name="DBitOutI.VI" Type="VI" URL="../DEVICEIO/Meilhaus/Universal Library/Das16.llb/DBitOutI.VI"/>
							<Item Name="DCFGBIT.VI" Type="VI" URL="../DEVICEIO/Meilhaus/Universal Library/Das16.llb/DCFGBIT.VI"/>
							<Item Name="DCFGPORT.VI" Type="VI" URL="../DEVICEIO/Meilhaus/Universal Library/Das16.llb/DCFGPORT.VI"/>
							<Item Name="DIn.VI" Type="VI" URL="../DEVICEIO/Meilhaus/Universal Library/Das16.llb/DIn.VI"/>
							<Item Name="DINSCBG.VI" Type="VI" URL="../DEVICEIO/Meilhaus/Universal Library/Das16.llb/DINSCBG.VI"/>
							<Item Name="DInScFg.VI" Type="VI" URL="../DEVICEIO/Meilhaus/Universal Library/Das16.llb/DInScFg.VI"/>
							<Item Name="DOut.VI" Type="VI" URL="../DEVICEIO/Meilhaus/Universal Library/Das16.llb/DOut.VI"/>
							<Item Name="DOUTSCBG.VI" Type="VI" URL="../DEVICEIO/Meilhaus/Universal Library/Das16.llb/DOUTSCBG.VI"/>
							<Item Name="DOutScFg.VI" Type="VI" URL="../DEVICEIO/Meilhaus/Universal Library/Das16.llb/DOutScFg.VI"/>
							<Item Name="ErrHdlng.VI" Type="VI" URL="../DEVICEIO/Meilhaus/Universal Library/Das16.llb/ErrHdlng.VI"/>
							<Item Name="ERRMSG.VI" Type="VI" URL="../DEVICEIO/Meilhaus/Universal Library/Das16.llb/ERRMSG.VI"/>
							<Item Name="FileAInS.VI" Type="VI" URL="../DEVICEIO/Meilhaus/Universal Library/Das16.llb/FileAInS.VI"/>
							<Item Name="FileInfo.VI" Type="VI" URL="../DEVICEIO/Meilhaus/Universal Library/Das16.llb/FileInfo.VI"/>
							<Item Name="FilePret.VI" Type="VI" URL="../DEVICEIO/Meilhaus/Universal Library/Das16.llb/FilePret.VI"/>
							<Item Name="FILEREAD.VI" Type="VI" URL="../DEVICEIO/Meilhaus/Universal Library/Das16.llb/FILEREAD.VI"/>
							<Item Name="FromEng(array).VI" Type="VI" URL="../DEVICEIO/Meilhaus/Universal Library/Das16.llb/FromEng(array).VI"/>
							<Item Name="FromEng(single).VI" Type="VI" URL="../DEVICEIO/Meilhaus/Universal Library/Das16.llb/FromEng(single).VI"/>
							<Item Name="FromEng(trigger).VI" Type="VI" URL="../DEVICEIO/Meilhaus/Universal Library/Das16.llb/FromEng(trigger).VI"/>
							<Item Name="FromEng.VI" Type="VI" URL="../DEVICEIO/Meilhaus/Universal Library/Das16.llb/FromEng.VI"/>
							<Item Name="GetBoard.VI" Type="VI" URL="../DEVICEIO/Meilhaus/Universal Library/Das16.llb/GetBoard.VI"/>
							<Item Name="GetCfg.VI" Type="VI" URL="../DEVICEIO/Meilhaus/Universal Library/Das16.llb/GetCfg.VI"/>
							<Item Name="GETSTAT.VI" Type="VI" URL="../DEVICEIO/Meilhaus/Universal Library/Das16.llb/GETSTAT.VI"/>
							<Item Name="InByte.VI" Type="VI" URL="../DEVICEIO/Meilhaus/Universal Library/Das16.llb/InByte.VI"/>
							<Item Name="InWord.VI" Type="VI" URL="../DEVICEIO/Meilhaus/Universal Library/Das16.llb/InWord.VI"/>
							<Item Name="MemRdPrt.VI" Type="VI" URL="../DEVICEIO/Meilhaus/Universal Library/Das16.llb/MemRdPrt.VI"/>
							<Item Name="MEMREAD.VI" Type="VI" URL="../DEVICEIO/Meilhaus/Universal Library/Das16.llb/MEMREAD.VI"/>
							<Item Name="MEMRESET.VI" Type="VI" URL="../DEVICEIO/Meilhaus/Universal Library/Das16.llb/MEMRESET.VI"/>
							<Item Name="MemSetDT.VI" Type="VI" URL="../DEVICEIO/Meilhaus/Universal Library/Das16.llb/MemSetDT.VI"/>
							<Item Name="MemWrite.VI" Type="VI" URL="../DEVICEIO/Meilhaus/Universal Library/Das16.llb/MemWrite.VI"/>
							<Item Name="OPTAIN.VI" Type="VI" URL="../DEVICEIO/Meilhaus/Universal Library/Das16.llb/OPTAIN.VI"/>
							<Item Name="OutByte.VI" Type="VI" URL="../DEVICEIO/Meilhaus/Universal Library/Das16.llb/OutByte.VI"/>
							<Item Name="OutWord.VI" Type="VI" URL="../DEVICEIO/Meilhaus/Universal Library/Das16.llb/OutWord.VI"/>
							<Item Name="RS485.VI" Type="VI" URL="../DEVICEIO/Meilhaus/Universal Library/Das16.llb/RS485.VI"/>
							<Item Name="ScaleArr.VI" Type="VI" URL="../DEVICEIO/Meilhaus/Universal Library/Das16.llb/ScaleArr.VI"/>
							<Item Name="ScalePnt.VI" Type="VI" URL="../DEVICEIO/Meilhaus/Universal Library/Das16.llb/ScalePnt.VI"/>
							<Item Name="SELCHAN.VI" Type="VI" URL="../DEVICEIO/Meilhaus/Universal Library/Das16.llb/SELCHAN.VI"/>
							<Item Name="SetCfg.VI" Type="VI" URL="../DEVICEIO/Meilhaus/Universal Library/Das16.llb/SetCfg.VI"/>
							<Item Name="Settrig.VI" Type="VI" URL="../DEVICEIO/Meilhaus/Universal Library/Das16.llb/Settrig.VI"/>
							<Item Name="STOPBG.VI" Type="VI" URL="../DEVICEIO/Meilhaus/Universal Library/Das16.llb/STOPBG.VI"/>
							<Item Name="TIn.VI" Type="VI" URL="../DEVICEIO/Meilhaus/Universal Library/Das16.llb/TIn.VI"/>
							<Item Name="TInScan.VI" Type="VI" URL="../DEVICEIO/Meilhaus/Universal Library/Das16.llb/TInScan.VI"/>
							<Item Name="ToEng(array).vi" Type="VI" URL="../DEVICEIO/Meilhaus/Universal Library/Das16.llb/ToEng(array).vi"/>
							<Item Name="ToEng(single).VI" Type="VI" URL="../DEVICEIO/Meilhaus/Universal Library/Das16.llb/ToEng(single).VI"/>
							<Item Name="ToEng.VI" Type="VI" URL="../DEVICEIO/Meilhaus/Universal Library/Das16.llb/ToEng.VI"/>
						</Item>
						<Item Name="82C54.pdf" Type="Document" URL="../DEVICEIO/Meilhaus/Universal Library/82C54.pdf"/>
						<Item Name="82C55A.pdf" Type="Document" URL="../DEVICEIO/Meilhaus/Universal Library/82C55A.pdf"/>
						<Item Name="7266.mnu" Type="Document" URL="../DEVICEIO/Meilhaus/Universal Library/7266.mnu"/>
						<Item Name="8254.mnu" Type="Document" URL="../DEVICEIO/Meilhaus/Universal Library/8254.mnu"/>
						<Item Name="8536.mnu" Type="Document" URL="../DEVICEIO/Meilhaus/Universal Library/8536.mnu"/>
						<Item Name="9513.mnu" Type="Document" URL="../DEVICEIO/Meilhaus/Universal Library/9513.mnu"/>
						<Item Name="9513A.pdf" Type="Document" URL="../DEVICEIO/Meilhaus/Universal Library/9513A.pdf"/>
						<Item Name="AnalogInput.mnu" Type="Document" URL="../DEVICEIO/Meilhaus/Universal Library/AnalogInput.mnu"/>
						<Item Name="AnalogOutput.mnu" Type="Document" URL="../DEVICEIO/Meilhaus/Universal Library/AnalogOutput.mnu"/>
						<Item Name="Cal_config.mnu" Type="Document" URL="../DEVICEIO/Meilhaus/Universal Library/Cal_config.mnu"/>
						<Item Name="Counter.mnu" Type="Document" URL="../DEVICEIO/Meilhaus/Universal Library/Counter.mnu"/>
						<Item Name="Digital_IO.mnu" Type="Document" URL="../DEVICEIO/Meilhaus/Universal Library/Digital_IO.mnu"/>
						<Item Name="LS7266R1.pdf" Type="Document" URL="../DEVICEIO/Meilhaus/Universal Library/LS7266R1.pdf"/>
						<Item Name="Memory.mnu" Type="Document" URL="../DEVICEIO/Meilhaus/Universal Library/Memory.mnu"/>
						<Item Name="ReadMe.txt" Type="Document" URL="../DEVICEIO/Meilhaus/Universal Library/ReadMe.txt"/>
						<Item Name="Serial.mnu" Type="Document" URL="../DEVICEIO/Meilhaus/Universal Library/Serial.mnu"/>
						<Item Name="Signal_Conditioning.mnu" Type="Document" URL="../DEVICEIO/Meilhaus/Universal Library/Signal_Conditioning.mnu"/>
						<Item Name="Streamer.mnu" Type="Document" URL="../DEVICEIO/Meilhaus/Universal Library/Streamer.mnu"/>
						<Item Name="ULLVHelp.chm" Type="Document" URL="../DEVICEIO/Meilhaus/Universal Library/ULLVHelp.chm"/>
						<Item Name="UniLib.chm" Type="Document" URL="../DEVICEIO/Meilhaus/Universal Library/UniLib.chm"/>
						<Item Name="Universal Library.mnu" Type="Document" URL="../DEVICEIO/Meilhaus/Universal Library/Universal Library.mnu"/>
					</Item>
					<Item Name="Meilhaus.lvlib" Type="Library" URL="../DEVICEIO/Meilhaus/Meilhaus.lvlib"/>
					<Item Name="Universal Library.txt" Type="Document" URL="../DEVICEIO/Meilhaus/Universal Library.txt"/>
				</Item>
				<Item Name="Mettler Toledo Balances" Type="Folder">
					<Item Name="Examples" Type="Folder">
						<Item Name="Mettler Toledo Balances Weight.vi" Type="VI" URL="../DEVICEIO/Mettler Toledo Balances/Examples/Mettler Toledo Balances Weight.vi"/>
						<Item Name="Mettler Toledo Balances.bin3" Type="Document" URL="../DEVICEIO/Mettler Toledo Balances/Examples/Mettler Toledo Balances.bin3"/>
					</Item>
					<Item Name="Private" Type="Folder"/>
					<Item Name="Public" Type="Folder">
						<Item Name="Action-Status" Type="Folder"/>
						<Item Name="Configure" Type="Folder">
							<Item Name="Advanced Parameters" Type="Folder"/>
							<Item Name="Applications" Type="Folder"/>
							<Item Name="Balance Settings" Type="Folder"/>
							<Item Name="Beeper" Type="Folder"/>
							<Item Name="Date - Time" Type="Folder"/>
							<Item Name="Tare-Zero-Calibration" Type="Folder"/>
							<Item Name="Weighing Parameters" Type="Folder"/>
						</Item>
						<Item Name="Data" Type="Folder"/>
						<Item Name="Utility" Type="Folder"/>
					</Item>
					<Item Name="Mettler Toledo Balances Readme.html" Type="Document" URL="../DEVICEIO/Mettler Toledo Balances/Mettler Toledo Balances Readme.html"/>
					<Item Name="Mettler Toledo Balances.aliases" Type="Document" URL="../DEVICEIO/Mettler Toledo Balances/Mettler Toledo Balances.aliases"/>
					<Item Name="Mettler Toledo Balances.lvlib" Type="Library" URL="../DEVICEIO/Mettler Toledo Balances/Mettler Toledo Balances.lvlib"/>
					<Item Name="Mettler Toledo Balances.lvproj" Type="Document" URL="../DEVICEIO/Mettler Toledo Balances/Mettler Toledo Balances.lvproj"/>
				</Item>
				<Item Name="Omron K3HB-HTA" Type="Folder">
					<Item Name="K3HB Library.lvlib" Type="Library" URL="../DEVICEIO/Omron K3HB-HTA/K3HB Library.lvlib"/>
					<Item Name="K3HB-HTA.vi" Type="VI" URL="../DEVICEIO/Omron K3HB-HTA/K3HB-HTA.vi"/>
				</Item>
				<Item Name="Mettler.vi" Type="VI" URL="../DEVICEIO/Mettler.vi"/>
				<Item Name="Mettler_IO task.ctl" Type="VI" URL="../DEVICEIO/Mettler_IO task.ctl"/>
				<Item Name="Mettler_IO.vi" Type="VI" URL="../DEVICEIO/Mettler_IO.vi"/>
				<Item Name="NestoMettlerCluster.ctl" Type="VI" URL="../DEVICEIO/NestoMettlerCluster.ctl"/>
				<Item Name="NestoOmronCluster.ctl" Type="VI" URL="../DEVICEIO/NestoOmronCluster.ctl"/>
				<Item Name="Omron.vi" Type="VI" URL="../DEVICEIO/Omron.vi"/>
				<Item Name="Omron_Sub.vi" Type="VI" URL="../DEVICEIO/Omron_Sub.vi"/>
			</Item>
			<Item Name="FILEIO" Type="Folder">
				<Item Name="TYPEDEF" Type="Folder">
					<Item Name="Analyse Project Tasks.ctl" Type="VI" URL="../FILEIO/TYPEDEF/Analyse Project Tasks.ctl"/>
					<Item Name="Report cluster.ctl" Type="VI" URL="../FILEIO/TYPEDEF/Report cluster.ctl"/>
					<Item Name="Select Project Info Cluster.ctl" Type="VI" URL="../FILEIO/TYPEDEF/Select Project Info Cluster.ctl"/>
				</Item>
				<Item Name="_Files and folders to check.vi" Type="VI" URL="../FILEIO/_Files and folders to check.vi"/>
				<Item Name="_Files and folders to copy.vi" Type="VI" URL="../FILEIO/_Files and folders to copy.vi"/>
				<Item Name="Analyse Project.vi" Type="VI" URL="../FILEIO/Analyse Project.vi"/>
				<Item Name="Copy files to app data.vi" Type="VI" URL="../FILEIO/Copy files to app data.vi"/>
				<Item Name="Create File Suffix String.vi" Type="VI" URL="../FILEIO/Create File Suffix String.vi"/>
				<Item Name="create folders.vi" Type="VI" URL="../FILEIO/create folders.vi"/>
				<Item Name="Create Project Directory.vi" Type="VI" URL="../FILEIO/Create Project Directory.vi"/>
				<Item Name="Create_correct_filename_and_extension.vi" Type="VI" URL="../FILEIO/Create_correct_filename_and_extension.vi"/>
				<Item Name="Directory of Top Level VI or exe.vi" Type="VI" URL="../FILEIO/Directory of Top Level VI or exe.vi"/>
				<Item Name="Find file from base directory.vi" Type="VI" URL="../FILEIO/Find file from base directory.vi"/>
				<Item Name="Find file.vi" Type="VI" URL="../FILEIO/Find file.vi"/>
				<Item Name="Open Project Load Test Data.vi" Type="VI" URL="../FILEIO/Open Project Load Test Data.vi"/>
				<Item Name="Project IO.vi" Type="VI" URL="../FILEIO/Project IO.vi"/>
				<Item Name="Read report settings.vi" Type="VI" URL="../FILEIO/Read report settings.vi"/>
				<Item Name="Read sensor settings.vi" Type="VI" URL="../FILEIO/Read sensor settings.vi"/>
				<Item Name="Recursive files from top directory.vi" Type="VI" URL="../FILEIO/Recursive files from top directory.vi"/>
				<Item Name="Report settings.vi" Type="VI" URL="../FILEIO/Report settings.vi"/>
				<Item Name="Report.vi" Type="VI" URL="../FILEIO/Report.vi"/>
				<Item Name="Report_sub.vi" Type="VI" URL="../FILEIO/Report_sub.vi"/>
				<Item Name="RSAT Directorries Ini IO.vi" Type="VI" URL="../FILEIO/RSAT Directorries Ini IO.vi"/>
				<Item Name="Select RSAT Directories.vi" Type="VI" URL="../FILEIO/Select RSAT Directories.vi"/>
				<Item Name="Sensor settings.vi" Type="VI" URL="../FILEIO/Sensor settings.vi"/>
				<Item Name="Sensor settings_sub.vi" Type="VI" URL="../FILEIO/Sensor settings_sub.vi"/>
				<Item Name="Set Report Paths.vi" Type="VI" URL="../FILEIO/Set Report Paths.vi"/>
				<Item Name="Settings IO.vi" Type="VI" URL="../FILEIO/Settings IO.vi"/>
				<Item Name="Version info.vi" Type="VI" URL="../FILEIO/Version info.vi"/>
				<Item Name="Write report settings.vi" Type="VI" URL="../FILEIO/Write report settings.vi"/>
				<Item Name="Write sensor settings.vi" Type="VI" URL="../FILEIO/Write sensor settings.vi"/>
			</Item>
			<Item Name="GLOBAL" Type="Folder">
				<Item Name="TYPEDEF" Type="Folder">
					<Item Name="Path Enum.ctl" Type="VI" URL="../GLOBAL/TYPEDEF/Path Enum.ctl"/>
					<Item Name="Report parameter cluster.ctl" Type="VI" URL="../GLOBAL/TYPEDEF/Report parameter cluster.ctl"/>
					<Item Name="RSAT parameter global cluster.ctl" Type="VI" URL="../GLOBAL/TYPEDEF/RSAT parameter global cluster.ctl"/>
					<Item Name="RSAT parameter global tasks.ctl" Type="VI" URL="../GLOBAL/TYPEDEF/RSAT parameter global tasks.ctl"/>
					<Item Name="Sensor parameter cluster.ctl" Type="VI" URL="../GLOBAL/TYPEDEF/Sensor parameter cluster.ctl"/>
					<Item Name="State global tasks.ctl" Type="VI" URL="../GLOBAL/TYPEDEF/State global tasks.ctl"/>
					<Item Name="states.ctl" Type="VI" URL="../GLOBAL/TYPEDEF/states.ctl"/>
					<Item Name="Task ReadWrite.ctl" Type="VI" URL="../GLOBAL/TYPEDEF/Task ReadWrite.ctl"/>
				</Item>
				<Item Name="NestoRD Simple Error Logger.vi" Type="VI" URL="../GLOBAL/NestoRD Simple Error Logger.vi"/>
				<Item Name="Path global.vi" Type="VI" URL="../GLOBAL/Path global.vi"/>
				<Item Name="RSAT Global Variables.vi" Type="VI" URL="../GLOBAL/RSAT Global Variables.vi"/>
				<Item Name="RSAT parameter global.vi" Type="VI" URL="../GLOBAL/RSAT parameter global.vi"/>
				<Item Name="RSAT reference global.vi" Type="VI" URL="../GLOBAL/RSAT reference global.vi"/>
				<Item Name="SensorAdjustValue.vi" Type="VI" URL="../PROCES/SensorAdjustValue.vi"/>
				<Item Name="State global.vi" Type="VI" URL="../GLOBAL/State global.vi"/>
			</Item>
			<Item Name="GUI" Type="Folder">
				<Item Name="TYPEDEF" Type="Folder">
					<Item Name="Inclination Status.ctl" Type="VI" URL="../GUI/TYPEDEF/Inclination Status.ctl"/>
					<Item Name="Report Settings GUI Ref Cluster.ctl" Type="VI" URL="../GUI/TYPEDEF/Report Settings GUI Ref Cluster.ctl"/>
					<Item Name="RSAT report.ico" Type="Document" URL="../GUI/TYPEDEF/RSAT report.ico"/>
					<Item Name="RSAT.ico" Type="Document" URL="../GUI/TYPEDEF/RSAT.ico"/>
					<Item Name="Sensor Settings GUI Ref Cluster.ctl" Type="VI" URL="../GUI/TYPEDEF/Sensor Settings GUI Ref Cluster.ctl"/>
				</Item>
				<Item Name="Exit Report Settings GUI.vi" Type="VI" URL="../GUI/TYPEDEF/Exit Report Settings GUI.vi"/>
				<Item Name="Exit Sensor Settings GUI.vi" Type="VI" URL="../GUI/TYPEDEF/Exit Sensor Settings GUI.vi"/>
				<Item Name="Init Report Settings GUI.vi" Type="VI" URL="../GUI/Init Report Settings GUI.vi"/>
				<Item Name="Init Sensor Settings GUI.vi" Type="VI" URL="../GUI/Init Sensor Settings GUI.vi"/>
				<Item Name="New Project.vi" Type="VI" URL="../GUI/New Project.vi"/>
				<Item Name="Report settings GUI.vi" Type="VI" URL="../GUI/Report settings GUI.vi"/>
				<Item Name="Sensor calibration GUI.vi" Type="VI" URL="../GUI/Sensor calibration GUI.vi"/>
				<Item Name="Sensor settings GUI.vi" Type="VI" URL="../GUI/Sensor settings GUI.vi"/>
			</Item>
			<Item Name="PROCES" Type="Folder">
				<Item Name="TYPEDEF" Type="Folder">
					<Item Name="PacerCluster.ctl" Type="VI" URL="../PROCES/TYPEDEF/PacerCluster.ctl"/>
					<Item Name="Plot Cluster.ctl" Type="VI" URL="../PROCES/TYPEDEF/Plot Cluster.ctl"/>
					<Item Name="Queue Payload Cluster.ctl" Type="VI" URL="../PROCES/TYPEDEF/Queue Payload Cluster.ctl"/>
					<Item Name="Queue Tasks.ctl" Type="VI" URL="../PROCES/TYPEDEF/Queue Tasks.ctl"/>
					<Item Name="Queue_sub names.ctl" Type="VI" URL="../PROCES/TYPEDEF/Queue_sub names.ctl"/>
				</Item>
				<Item Name="Angle Trigger Plot.vi" Type="VI" URL="../PROCES/Angle Trigger Plot.vi"/>
				<Item Name="AngleTriggerPlotV2.vi" Type="VI" URL="../PROCES/AngleTriggerPlotV2.vi"/>
				<Item Name="Append Information Control.vi" Type="VI" URL="../PROCES/Append Information Control.vi"/>
				<Item Name="CloseScreenPlots.vi" Type="VI" URL="../PROCES/CloseScreenPlots.vi"/>
				<Item Name="Execute VI.vi" Type="VI" URL="../PROCES/Execute VI.vi"/>
				<Item Name="Exit heartbeat.vi" Type="VI" URL="../PROCES/Exit heartbeat.vi"/>
				<Item Name="Exit.vi" Type="VI" URL="../PROCES/Exit.vi"/>
				<Item Name="Init heartbeat.vi" Type="VI" URL="../PROCES/Init heartbeat.vi"/>
				<Item Name="Init.vi" Type="VI" URL="../PROCES/Init.vi"/>
				<Item Name="Initialize Angle Threshold and Weight Value.vi" Type="VI" URL="../PROCES/Initialize Angle Threshold and Weight Value.vi"/>
				<Item Name="Make Plot.vi" Type="VI" URL="../PROCES/Make Plot.vi"/>
				<Item Name="MeasurementPacer.vi" Type="VI" URL="../PROCES/MeasurementPacer.vi"/>
				<Item Name="MeasurementPacer_Sub.vi" Type="VI" URL="../PROCES/MeasurementPacer_Sub.vi"/>
				<Item Name="Monitor Angle.vi" Type="VI" URL="../PROCES/Monitor Angle.vi"/>
				<Item Name="MonitorAngleV2.vi" Type="VI" URL="../PROCES/MonitorAngleV2.vi"/>
				<Item Name="OpenScreenPlots.vi" Type="VI" URL="../PROCES/OpenScreenPlots.vi"/>
				<Item Name="Pacer Timer Loop.vi" Type="VI" URL="../PROCES/Pacer Timer Loop.vi"/>
				<Item Name="Plot.vi" Type="VI" URL="../PROCES/Plot.vi"/>
				<Item Name="Plot_sub.vi" Type="VI" URL="../PROCES/Plot_sub.vi"/>
				<Item Name="Proces Time.vi" Type="VI" URL="../PROCES/Proces Time.vi"/>
				<Item Name="ProcesInfoString.vi" Type="VI" URL="../PROCES/ProcesInfoString.vi"/>
				<Item Name="Project IO sub.vi" Type="VI" URL="../PROCES/Project IO sub.vi"/>
				<Item Name="Qeueu_sub.vi" Type="VI" URL="../PROCES/Qeueu_sub.vi"/>
				<Item Name="Qeueu_sub_receive_chain.vi" Type="VI" URL="../PROCES/Qeueu_sub_receive_chain.vi"/>
				<Item Name="Quality Monitor.vi" Type="VI" URL="../PROCES/Quality Monitor.vi"/>
				<Item Name="RoundToNdigits.vi" Type="VI" URL="../PROCES/RoundToNdigits.vi"/>
				<Item Name="RSAT Menu.vi" Type="VI" URL="../PROCES/RSAT Menu.vi"/>
				<Item Name="RTM.vi" Type="VI" URL="../PROCES/RTM.vi"/>
				<Item Name="SendInfo2GUI.vi" Type="VI" URL="../PROCES/SendInfo2GUI.vi"/>
				<Item Name="Set Menu.vi" Type="VI" URL="../PROCES/Set Menu.vi"/>
				<Item Name="Set UI.vi" Type="VI" URL="../PROCES/Set UI.vi"/>
				<Item Name="StateManager.vi" Type="VI" URL="../PROCES/StateManager.vi"/>
				<Item Name="Time conversion.vi" Type="VI" URL="../PROCES/Time conversion.vi"/>
				<Item Name="Timer Is Zero.vi" Type="VI" URL="../PROCES/Timer Is Zero.vi"/>
				<Item Name="UII.vi" Type="VI" URL="../PROCES/UII.vi"/>
			</Item>
			<Item Name="TESTROUTINES" Type="Folder" URL="../TESTROUTINES">
				<Property Name="NI.DISK" Type="Bool">true</Property>
			</Item>
			<Item Name="Analyse_string2values.vi" Type="VI" URL="../Analyse_string2values.vi"/>
			<Item Name="default_report.smtd" Type="Document" URL="../default_report.smtd"/>
			<Item Name="default_sensor.smtd" Type="Document" URL="../default_sensor.smtd"/>
			<Item Name="recent_report.smtd" Type="Document" URL="../recent_report.smtd"/>
			<Item Name="recent_sensor.smtd" Type="Document" URL="../recent_sensor.smtd"/>
			<Item Name="RSAT Main.vi" Type="VI" URL="../RSAT Main.vi"/>
			<Item Name="RSAT.aliases" Type="Document" URL="../RSAT.aliases"/>
			<Item Name="RSAT.lvlps" Type="Document" URL="../RSAT.lvlps"/>
			<Item Name="RSAT.rtm" Type="Document" URL="../RSAT.rtm"/>
			<Item Name="RSATcluster.ctl" Type="VI" URL="../RSATcluster.ctl"/>
		</Item>
		<Item Name="Somatidio Logo.ico" Type="Document" URL="../Somatidio Logo.ico"/>
		<Item Name="Dependencies" Type="Dependencies">
			<Item Name="vi.lib" Type="Folder">
				<Item Name="8.6CompatibleGlobalVar.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/config.llb/8.6CompatibleGlobalVar.vi"/>
				<Item Name="Application Directory.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Application Directory.vi"/>
				<Item Name="Bit-array To Byte-array.vi" Type="VI" URL="/&lt;vilib&gt;/picture/pictutil.llb/Bit-array To Byte-array.vi"/>
				<Item Name="BuildErrorSource.vi" Type="VI" URL="/&lt;vilib&gt;/Platform/fileVersionInfo.llb/BuildErrorSource.vi"/>
				<Item Name="BuildHelpPath.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/BuildHelpPath.vi"/>
				<Item Name="Built App File Layout.vi" Type="VI" URL="/&lt;vilib&gt;/AppBuilder/Built App File Layout.vi"/>
				<Item Name="Calc Long Word Padded Width.vi" Type="VI" URL="/&lt;vilib&gt;/picture/bmp.llb/Calc Long Word Padded Width.vi"/>
				<Item Name="Check Color Table Size.vi" Type="VI" URL="/&lt;vilib&gt;/picture/jpeg.llb/Check Color Table Size.vi"/>
				<Item Name="Check Data Size.vi" Type="VI" URL="/&lt;vilib&gt;/picture/jpeg.llb/Check Data Size.vi"/>
				<Item Name="Check File Permissions.vi" Type="VI" URL="/&lt;vilib&gt;/picture/jpeg.llb/Check File Permissions.vi"/>
				<Item Name="Check if File or Folder Exists.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/libraryn.llb/Check if File or Folder Exists.vi"/>
				<Item Name="Check Path.vi" Type="VI" URL="/&lt;vilib&gt;/picture/jpeg.llb/Check Path.vi"/>
				<Item Name="Check Special Tags.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Check Special Tags.vi"/>
				<Item Name="Clear Errors.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Clear Errors.vi"/>
				<Item Name="Close File+.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Close File+.vi"/>
				<Item Name="Close Registry Key.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Close Registry Key.vi"/>
				<Item Name="Color to RGB.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/colorconv.llb/Color to RGB.vi"/>
				<Item Name="compatCalcOffset.vi" Type="VI" URL="/&lt;vilib&gt;/_oldvers/_oldvers.llb/compatCalcOffset.vi"/>
				<Item Name="compatFileDialog.vi" Type="VI" URL="/&lt;vilib&gt;/_oldvers/_oldvers.llb/compatFileDialog.vi"/>
				<Item Name="compatOpenFileOperation.vi" Type="VI" URL="/&lt;vilib&gt;/_oldvers/_oldvers.llb/compatOpenFileOperation.vi"/>
				<Item Name="compatOverwrite.vi" Type="VI" URL="/&lt;vilib&gt;/_oldvers/_oldvers.llb/compatOverwrite.vi"/>
				<Item Name="compatReadText.vi" Type="VI" URL="/&lt;vilib&gt;/_oldvers/_oldvers.llb/compatReadText.vi"/>
				<Item Name="Convert property node font to graphics font.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Convert property node font to graphics font.vi"/>
				<Item Name="Create ActiveX Event Queue.vi" Type="VI" URL="/&lt;vilib&gt;/Platform/ax-events.llb/Create ActiveX Event Queue.vi"/>
				<Item Name="Create Error Clust.vi" Type="VI" URL="/&lt;vilib&gt;/Platform/ax-events.llb/Create Error Clust.vi"/>
				<Item Name="Create Mask By Alpha.vi" Type="VI" URL="/&lt;vilib&gt;/picture/picture.llb/Create Mask By Alpha.vi"/>
				<Item Name="Create Queue.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/queue.llb/Create Queue.vi"/>
				<Item Name="Destroy ActiveX Event Queue.vi" Type="VI" URL="/&lt;vilib&gt;/Platform/ax-events.llb/Destroy ActiveX Event Queue.vi"/>
				<Item Name="Destroy Queue.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/queue.llb/Destroy Queue.vi"/>
				<Item Name="Details Display Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Details Display Dialog.vi"/>
				<Item Name="DialogType.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/DialogType.ctl"/>
				<Item Name="DialogTypeEnum.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/DialogTypeEnum.ctl"/>
				<Item Name="Directory of Top Level VI.vi" Type="VI" URL="/&lt;vilib&gt;/picture/jpeg.llb/Directory of Top Level VI.vi"/>
				<Item Name="Error Cluster From Error Code.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Error Cluster From Error Code.vi"/>
				<Item Name="Error Code Database.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Error Code Database.vi"/>
				<Item Name="ErrWarn.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/ErrWarn.ctl"/>
				<Item Name="Escape Characters for HTTP.vi" Type="VI" URL="/&lt;vilib&gt;/printing/PathToURL.llb/Escape Characters for HTTP.vi"/>
				<Item Name="EventData.ctl" Type="VI" URL="/&lt;vilib&gt;/Platform/ax-events.llb/EventData.ctl"/>
				<Item Name="eventvkey.ctl" Type="VI" URL="/&lt;vilib&gt;/event_ctls.llb/eventvkey.ctl"/>
				<Item Name="ex_CorrectErrorChain.vi" Type="VI" URL="/&lt;vilib&gt;/express/express shared/ex_CorrectErrorChain.vi"/>
				<Item Name="FileVersionInfo.vi" Type="VI" URL="/&lt;vilib&gt;/Platform/fileVersionInfo.llb/FileVersionInfo.vi"/>
				<Item Name="FileVersionInformation.ctl" Type="VI" URL="/&lt;vilib&gt;/Platform/fileVersionInfo.llb/FileVersionInformation.ctl"/>
				<Item Name="Find First Error.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Find First Error.vi"/>
				<Item Name="Find Tag.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Find Tag.vi"/>
				<Item Name="FixedFileInfo_Struct.ctl" Type="VI" URL="/&lt;vilib&gt;/Platform/fileVersionInfo.llb/FixedFileInfo_Struct.ctl"/>
				<Item Name="Flip and Pad for Picture Control.vi" Type="VI" URL="/&lt;vilib&gt;/picture/bmp.llb/Flip and Pad for Picture Control.vi"/>
				<Item Name="Flush Queue.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/queue.llb/Flush Queue.vi"/>
				<Item Name="Format Message String.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Format Message String.vi"/>
				<Item Name="General Error Handler Core CORE.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/General Error Handler Core CORE.vi"/>
				<Item Name="General Error Handler.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/General Error Handler.vi"/>
				<Item Name="Generate Temporary File Path.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/libraryn.llb/Generate Temporary File Path.vi"/>
				<Item Name="Get File Extension.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/libraryn.llb/Get File Extension.vi"/>
				<Item Name="Get Instrument State.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/victl.llb/Get Instrument State.vi"/>
				<Item Name="Get LV Class Default Value.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/LVClass/Get LV Class Default Value.vi"/>
				<Item Name="Get Queue Status.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/queue.llb/Get Queue Status.vi"/>
				<Item Name="Get String Text Bounds.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Get String Text Bounds.vi"/>
				<Item Name="Get System Directory.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/sysdir.llb/Get System Directory.vi"/>
				<Item Name="Get Text Rect.vi" Type="VI" URL="/&lt;vilib&gt;/picture/picture.llb/Get Text Rect.vi"/>
				<Item Name="GetFileVersionInfo.vi" Type="VI" URL="/&lt;vilib&gt;/Platform/fileVersionInfo.llb/GetFileVersionInfo.vi"/>
				<Item Name="GetFileVersionInfoSize.vi" Type="VI" URL="/&lt;vilib&gt;/Platform/fileVersionInfo.llb/GetFileVersionInfoSize.vi"/>
				<Item Name="GetHelpDir.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/GetHelpDir.vi"/>
				<Item Name="GetRTHostConnectedProp.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/GetRTHostConnectedProp.vi"/>
				<Item Name="imagedata.ctl" Type="VI" URL="/&lt;vilib&gt;/picture/picture.llb/imagedata.ctl"/>
				<Item Name="Insert Queue Element.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/queue.llb/Insert Queue Element.vi"/>
				<Item Name="List Directory and LLBs.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/libraryn.llb/List Directory and LLBs.vi"/>
				<Item Name="Longest Line Length in Pixels.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Longest Line Length in Pixels.vi"/>
				<Item Name="LVBoundsTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVBoundsTypeDef.ctl"/>
				<Item Name="LVRectTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVRectTypeDef.ctl"/>
				<Item Name="MoveMemory.vi" Type="VI" URL="/&lt;vilib&gt;/Platform/fileVersionInfo.llb/MoveMemory.vi"/>
				<Item Name="NI_AALBase.lvlib" Type="Library" URL="/&lt;vilib&gt;/Analysis/NI_AALBase.lvlib"/>
				<Item Name="NI_AALPro.lvlib" Type="Library" URL="/&lt;vilib&gt;/Analysis/NI_AALPro.lvlib"/>
				<Item Name="NI_FileType.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/lvfile.llb/NI_FileType.lvlib"/>
				<Item Name="NI_HTML.lvclass" Type="LVClass" URL="/&lt;vilib&gt;/Utility/NIReport.llb/HTML/NI_HTML.lvclass"/>
				<Item Name="NI_LVConfig.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/config.llb/NI_LVConfig.lvlib"/>
				<Item Name="NI_PackedLibraryUtility.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/LVLibp/NI_PackedLibraryUtility.lvlib"/>
				<Item Name="NI_report.lvclass" Type="LVClass" URL="/&lt;vilib&gt;/Utility/NIReport.llb/NI_report.lvclass"/>
				<Item Name="NI_ReportGenerationCore.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/NIReport.llb/NI_ReportGenerationCore.lvlib"/>
				<Item Name="NI_Standard Report.lvclass" Type="LVClass" URL="/&lt;vilib&gt;/Utility/NIReport.llb/Standard Report/NI_Standard Report.lvclass"/>
				<Item Name="Not Found Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Not Found Dialog.vi"/>
				<Item Name="OccFireType.ctl" Type="VI" URL="/&lt;vilib&gt;/Platform/ax-events.llb/OccFireType.ctl"/>
				<Item Name="Open File+.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Open File+.vi"/>
				<Item Name="Open Panel.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/victl.llb/Open Panel.vi"/>
				<Item Name="Open Registry Key.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Open Registry Key.vi"/>
				<Item Name="Open_Create_Replace File.vi" Type="VI" URL="/&lt;vilib&gt;/_oldvers/_oldvers.llb/Open_Create_Replace File.vi"/>
				<Item Name="Path to URL.vi" Type="VI" URL="/&lt;vilib&gt;/printing/PathToURL.llb/Path to URL.vi"/>
				<Item Name="Preload Instrument.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/victl.llb/Preload Instrument.vi"/>
				<Item Name="Queue RefNum" Type="VI" URL="/&lt;vilib&gt;/Utility/queue.llb/Queue RefNum"/>
				<Item Name="Queue SRdB command.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/queue.llb/Queue SRdB command.ctl"/>
				<Item Name="Queue SRdB.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/queue.llb/Queue SRdB.vi"/>
				<Item Name="Read Delimited Spreadsheet (DBL).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Read Delimited Spreadsheet (DBL).vi"/>
				<Item Name="Read Delimited Spreadsheet (I64).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Read Delimited Spreadsheet (I64).vi"/>
				<Item Name="Read Delimited Spreadsheet (string).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Read Delimited Spreadsheet (string).vi"/>
				<Item Name="Read Delimited Spreadsheet.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Read Delimited Spreadsheet.vi"/>
				<Item Name="Read File+ (string).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Read File+ (string).vi"/>
				<Item Name="Read From Spreadsheet File (DBL).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Read From Spreadsheet File (DBL).vi"/>
				<Item Name="Read From Spreadsheet File (I64).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Read From Spreadsheet File (I64).vi"/>
				<Item Name="Read From Spreadsheet File (string).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Read From Spreadsheet File (string).vi"/>
				<Item Name="Read From Spreadsheet File.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Read From Spreadsheet File.vi"/>
				<Item Name="Read Lines From File (with error IO).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Read Lines From File (with error IO).vi"/>
				<Item Name="Read Lines From File.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Read Lines From File.vi"/>
				<Item Name="Read PNG File.vi" Type="VI" URL="/&lt;vilib&gt;/picture/png.llb/Read PNG File.vi"/>
				<Item Name="Read Registry Value DWORD.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Read Registry Value DWORD.vi"/>
				<Item Name="Read Registry Value Simple STR.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Read Registry Value Simple STR.vi"/>
				<Item Name="Read Registry Value Simple U32.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Read Registry Value Simple U32.vi"/>
				<Item Name="Read Registry Value Simple.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Read Registry Value Simple.vi"/>
				<Item Name="Read Registry Value STR.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Read Registry Value STR.vi"/>
				<Item Name="Read Registry Value.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Read Registry Value.vi"/>
				<Item Name="Recursive File List.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/libraryn.llb/Recursive File List.vi"/>
				<Item Name="Registry Handle Master.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Registry Handle Master.vi"/>
				<Item Name="Registry refnum.ctl" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Registry refnum.ctl"/>
				<Item Name="Registry RtKey.ctl" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Registry RtKey.ctl"/>
				<Item Name="Registry SAM.ctl" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Registry SAM.ctl"/>
				<Item Name="Registry Simplify Data Type.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Registry Simplify Data Type.vi"/>
				<Item Name="Registry View.ctl" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Registry View.ctl"/>
				<Item Name="Registry WinErr-LVErr.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Registry WinErr-LVErr.vi"/>
				<Item Name="Remove Queue Element.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/queue.llb/Remove Queue Element.vi"/>
				<Item Name="Run Instrument.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/victl.llb/Run Instrument.vi"/>
				<Item Name="Search and Replace Pattern.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Search and Replace Pattern.vi"/>
				<Item Name="Set Bold Text.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Set Bold Text.vi"/>
				<Item Name="Set String Value.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Set String Value.vi"/>
				<Item Name="Simple Error Handler.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Simple Error Handler.vi"/>
				<Item Name="Space Constant.vi" Type="VI" URL="/&lt;vilib&gt;/dlg_ctls.llb/Space Constant.vi"/>
				<Item Name="STR_ASCII-Unicode.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/STR_ASCII-Unicode.vi"/>
				<Item Name="subFile Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/express/express input/FileDialogBlock.llb/subFile Dialog.vi"/>
				<Item Name="System Directory Type.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/sysdir.llb/System Directory Type.ctl"/>
				<Item Name="TagReturnType.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/TagReturnType.ctl"/>
				<Item Name="Three Button Dialog CORE.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Three Button Dialog CORE.vi"/>
				<Item Name="Three Button Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Three Button Dialog.vi"/>
				<Item Name="Trim Whitespace.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Trim Whitespace.vi"/>
				<Item Name="VerQueryValue.vi" Type="VI" URL="/&lt;vilib&gt;/Platform/fileVersionInfo.llb/VerQueryValue.vi"/>
				<Item Name="viRef buffer.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/victl.llb/viRef buffer.vi"/>
				<Item Name="VISA Configure Serial Port" Type="VI" URL="/&lt;vilib&gt;/Instr/_visa.llb/VISA Configure Serial Port"/>
				<Item Name="VISA Configure Serial Port (Instr).vi" Type="VI" URL="/&lt;vilib&gt;/Instr/_visa.llb/VISA Configure Serial Port (Instr).vi"/>
				<Item Name="VISA Configure Serial Port (Serial Instr).vi" Type="VI" URL="/&lt;vilib&gt;/Instr/_visa.llb/VISA Configure Serial Port (Serial Instr).vi"/>
				<Item Name="Wait On ActiveX Event.vi" Type="VI" URL="/&lt;vilib&gt;/Platform/ax-events.llb/Wait On ActiveX Event.vi"/>
				<Item Name="Wait types.ctl" Type="VI" URL="/&lt;vilib&gt;/Platform/ax-events.llb/Wait types.ctl"/>
				<Item Name="whitespace.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/whitespace.ctl"/>
				<Item Name="Write BMP Data To Buffer.vi" Type="VI" URL="/&lt;vilib&gt;/picture/bmp.llb/Write BMP Data To Buffer.vi"/>
				<Item Name="Write BMP Data.vi" Type="VI" URL="/&lt;vilib&gt;/picture/bmp.llb/Write BMP Data.vi"/>
				<Item Name="Write BMP File.vi" Type="VI" URL="/&lt;vilib&gt;/picture/bmp.llb/Write BMP File.vi"/>
				<Item Name="Write JPEG File.vi" Type="VI" URL="/&lt;vilib&gt;/picture/jpeg.llb/Write JPEG File.vi"/>
				<Item Name="Write PNG File.vi" Type="VI" URL="/&lt;vilib&gt;/picture/png.llb/Write PNG File.vi"/>
			</Item>
			<Item Name="Advapi32.dll" Type="Document" URL="Advapi32.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="kernel32.dll" Type="Document" URL="kernel32.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="lvanlys.dll" Type="Document" URL="/&lt;resource&gt;/lvanlys.dll"/>
			<Item Name="version.dll" Type="Document" URL="version.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
		</Item>
		<Item Name="Build Specifications" Type="Build">
			<Item Name="RSAT Acquire" Type="EXE">
				<Property Name="App_copyErrors" Type="Bool">true</Property>
				<Property Name="App_INI_aliasGUID" Type="Str">{76F5D45B-D405-44FE-B767-CF10822559C6}</Property>
				<Property Name="App_INI_GUID" Type="Str">{DC02644D-CC5E-4C28-8C56-1AA17D602C22}</Property>
				<Property Name="App_serverConfig.httpPort" Type="Int">8002</Property>
				<Property Name="Bld_autoIncrement" Type="Bool">true</Property>
				<Property Name="Bld_buildCacheID" Type="Str">{4037273D-1FB9-4AAE-A99A-5F85702AE81D}</Property>
				<Property Name="Bld_buildSpecName" Type="Str">RSAT Acquire</Property>
				<Property Name="Bld_excludeLibraryItems" Type="Bool">true</Property>
				<Property Name="Bld_excludePolymorphicVIs" Type="Bool">true</Property>
				<Property Name="Bld_localDestDir" Type="Path">../builds/NI_AB_PROJECTNAME/RSAT Acquire</Property>
				<Property Name="Bld_localDestDirType" Type="Str">relativeToCommon</Property>
				<Property Name="Bld_modifyLibraryFile" Type="Bool">true</Property>
				<Property Name="Bld_previewCacheID" Type="Str">{E05E3650-E37D-4BCA-BEBB-4DE286678194}</Property>
				<Property Name="Bld_version.build" Type="Int">95</Property>
				<Property Name="Bld_version.major" Type="Int">1</Property>
				<Property Name="Destination[0].destName" Type="Str">RSAT Acquire.exe</Property>
				<Property Name="Destination[0].path" Type="Path">../builds/NI_AB_PROJECTNAME/RSAT Acquire/RSAT Acquire.exe</Property>
				<Property Name="Destination[0].preserveHierarchy" Type="Bool">true</Property>
				<Property Name="Destination[0].type" Type="Str">App</Property>
				<Property Name="Destination[1].destName" Type="Str">Support Directory</Property>
				<Property Name="Destination[1].path" Type="Path">../builds/NI_AB_PROJECTNAME/RSAT Acquire/data</Property>
				<Property Name="DestinationCount" Type="Int">2</Property>
				<Property Name="Exe_iconItemID" Type="Ref">/My Computer/Somatidio Logo.ico</Property>
				<Property Name="Source[0].itemID" Type="Str">{9C557411-397B-4B6E-B0BA-D4CE00192577}</Property>
				<Property Name="Source[0].type" Type="Str">Container</Property>
				<Property Name="Source[1].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[1].itemID" Type="Ref">/My Computer/SRC/RSAT Main.vi</Property>
				<Property Name="Source[1].sourceInclusion" Type="Str">TopLevel</Property>
				<Property Name="Source[1].type" Type="Str">VI</Property>
				<Property Name="Source[2].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[2].itemID" Type="Ref">/My Computer/SRC/default_report.smtd</Property>
				<Property Name="Source[2].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[3].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[3].itemID" Type="Ref">/My Computer/SRC/default_sensor.smtd</Property>
				<Property Name="Source[3].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[4].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[4].itemID" Type="Ref">/My Computer/SRC/recent_report.smtd</Property>
				<Property Name="Source[4].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[5].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[5].itemID" Type="Ref">/My Computer/SRC/recent_sensor.smtd</Property>
				<Property Name="Source[5].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[6].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[6].itemID" Type="Ref">/My Computer/SRC/GUI/New Project.vi</Property>
				<Property Name="Source[6].type" Type="Str">VI</Property>
				<Property Name="Source[7].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[7].itemID" Type="Ref">/My Computer/SRC/GUI/Report settings GUI.vi</Property>
				<Property Name="Source[7].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[7].type" Type="Str">VI</Property>
				<Property Name="Source[8].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[8].itemID" Type="Ref">/My Computer/SRC/GUI/Sensor settings GUI.vi</Property>
				<Property Name="Source[8].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[8].type" Type="Str">VI</Property>
				<Property Name="SourceCount" Type="Int">9</Property>
				<Property Name="TgtF_companyName" Type="Str">SOMATIDIO / Nesto R&amp;D</Property>
				<Property Name="TgtF_fileDescription" Type="Str">RSAT Acquire</Property>
				<Property Name="TgtF_internalName" Type="Str">RSAT Acquire</Property>
				<Property Name="TgtF_legalCopyright" Type="Str">Copyright © 2019</Property>
				<Property Name="TgtF_productName" Type="Str">RSAT Acquire</Property>
				<Property Name="TgtF_targetfileGUID" Type="Str">{0B660A33-4888-40EC-A72F-8E81FA66BA6C}</Property>
				<Property Name="TgtF_targetfileName" Type="Str">RSAT Acquire.exe</Property>
			</Item>
			<Item Name="RSAT Acquire Installer" Type="Installer">
				<Property Name="Destination[0].name" Type="Str">RSAT</Property>
				<Property Name="Destination[0].parent" Type="Str">{3912416A-D2E5-411B-AFEE-B63654D690C0}</Property>
				<Property Name="Destination[0].tag" Type="Str">{1774A4E3-C74F-41D1-AD07-EB58D4DC6914}</Property>
				<Property Name="Destination[0].type" Type="Str">userFolder</Property>
				<Property Name="Destination[1].name" Type="Str">RSAT Acquire</Property>
				<Property Name="Destination[1].parent" Type="Str">{1774A4E3-C74F-41D1-AD07-EB58D4DC6914}</Property>
				<Property Name="Destination[1].tag" Type="Str">{3833823B-68A5-4F98-A59C-7C63B22057CF}</Property>
				<Property Name="Destination[1].type" Type="Str">userFolder</Property>
				<Property Name="DestinationCount" Type="Int">2</Property>
				<Property Name="DistPart[0].flavorID" Type="Str">_full_</Property>
				<Property Name="DistPart[0].productID" Type="Str">{5E17F5AF-F47C-4C3A-A219-91F4638CE9D1}</Property>
				<Property Name="DistPart[0].productName" Type="Str">NI-Serial Runtime 17.0</Property>
				<Property Name="DistPart[0].upgradeCode" Type="Str">{01D82F43-B48D-46FF-8601-FC4FAAE20F41}</Property>
				<Property Name="DistPart[1].flavorID" Type="Str">_deployment_</Property>
				<Property Name="DistPart[1].productID" Type="Str">{F9B5B433-547E-4A74-AFE6-91C16787824E}</Property>
				<Property Name="DistPart[1].productName" Type="Str">NI-VISA Runtime 17.0</Property>
				<Property Name="DistPart[1].upgradeCode" Type="Str">{8627993A-3F66-483C-A562-0D3BA3F267B1}</Property>
				<Property Name="DistPart[2].flavorID" Type="Str">DefaultFull</Property>
				<Property Name="DistPart[2].productID" Type="Str">{AF2FEF05-E895-4750-8F69-B5FA5388B2A3}</Property>
				<Property Name="DistPart[2].productName" Type="Str">NI LabVIEW Runtime 2017</Property>
				<Property Name="DistPart[2].SoftDep[0].exclude" Type="Bool">false</Property>
				<Property Name="DistPart[2].SoftDep[0].productName" Type="Str">NI LabVIEW Runtime 2017 Non-English Support.</Property>
				<Property Name="DistPart[2].SoftDep[0].upgradeCode" Type="Str">{182AE811-85B6-4238-B67E-F19497CC186B}</Property>
				<Property Name="DistPart[2].SoftDep[1].exclude" Type="Bool">false</Property>
				<Property Name="DistPart[2].SoftDep[1].productName" Type="Str">NI ActiveX Container</Property>
				<Property Name="DistPart[2].SoftDep[1].upgradeCode" Type="Str">{1038A887-23E1-4289-B0BD-0C4B83C6BA21}</Property>
				<Property Name="DistPart[2].SoftDep[10].exclude" Type="Bool">false</Property>
				<Property Name="DistPart[2].SoftDep[10].productName" Type="Str">NI mDNS Responder 14.0</Property>
				<Property Name="DistPart[2].SoftDep[10].upgradeCode" Type="Str">{9607874B-4BB3-42CB-B450-A2F5EF60BA3B}</Property>
				<Property Name="DistPart[2].SoftDep[11].exclude" Type="Bool">false</Property>
				<Property Name="DistPart[2].SoftDep[11].productName" Type="Str">NI Deployment Framework 2017</Property>
				<Property Name="DistPart[2].SoftDep[11].upgradeCode" Type="Str">{838942E4-B73C-492E-81A3-AA1E291FD0DC}</Property>
				<Property Name="DistPart[2].SoftDep[12].exclude" Type="Bool">false</Property>
				<Property Name="DistPart[2].SoftDep[12].productName" Type="Str">NI Error Reporting 2017</Property>
				<Property Name="DistPart[2].SoftDep[12].upgradeCode" Type="Str">{42E818C6-2B08-4DE7-BD91-B0FD704C119A}</Property>
				<Property Name="DistPart[2].SoftDep[2].exclude" Type="Bool">false</Property>
				<Property Name="DistPart[2].SoftDep[2].productName" Type="Str">Math Kernel Libraries</Property>
				<Property Name="DistPart[2].SoftDep[2].upgradeCode" Type="Str">{699C1AC5-2CF2-4745-9674-B19536EBA8A3}</Property>
				<Property Name="DistPart[2].SoftDep[3].exclude" Type="Bool">false</Property>
				<Property Name="DistPart[2].SoftDep[3].productName" Type="Str">NI Logos 5.9</Property>
				<Property Name="DistPart[2].SoftDep[3].upgradeCode" Type="Str">{5E4A4CE3-4D06-11D4-8B22-006008C16337}</Property>
				<Property Name="DistPart[2].SoftDep[4].exclude" Type="Bool">false</Property>
				<Property Name="DistPart[2].SoftDep[4].productName" Type="Str">NI TDM Streaming 17.0</Property>
				<Property Name="DistPart[2].SoftDep[4].upgradeCode" Type="Str">{4CD11BE6-6BB7-4082-8A27-C13771BC309B}</Property>
				<Property Name="DistPart[2].SoftDep[5].exclude" Type="Bool">false</Property>
				<Property Name="DistPart[2].SoftDep[5].productName" Type="Str">NI LabVIEW Web Server 2017</Property>
				<Property Name="DistPart[2].SoftDep[5].upgradeCode" Type="Str">{0960380B-EA86-4E0C-8B57-14CD8CCF2C15}</Property>
				<Property Name="DistPart[2].SoftDep[6].exclude" Type="Bool">false</Property>
				<Property Name="DistPart[2].SoftDep[6].productName" Type="Str">NI LabVIEW Real-Time NBFifo 2017</Property>
				<Property Name="DistPart[2].SoftDep[6].upgradeCode" Type="Str">{4F261250-2C38-488D-A9EC-9D1EFCC24D4B}</Property>
				<Property Name="DistPart[2].SoftDep[7].exclude" Type="Bool">false</Property>
				<Property Name="DistPart[2].SoftDep[7].productName" Type="Str">NI VC2008MSMs</Property>
				<Property Name="DistPart[2].SoftDep[7].upgradeCode" Type="Str">{FDA3F8BB-BAA9-45D7-8DC7-22E1F5C76315}</Property>
				<Property Name="DistPart[2].SoftDep[8].exclude" Type="Bool">false</Property>
				<Property Name="DistPart[2].SoftDep[8].productName" Type="Str">NI VC2010MSMs</Property>
				<Property Name="DistPart[2].SoftDep[8].upgradeCode" Type="Str">{EFBA6F9E-F934-4BD7-AC51-60CCA480489C}</Property>
				<Property Name="DistPart[2].SoftDep[9].exclude" Type="Bool">false</Property>
				<Property Name="DistPart[2].SoftDep[9].productName" Type="Str">NI VC2015 Runtime</Property>
				<Property Name="DistPart[2].SoftDep[9].upgradeCode" Type="Str">{D42E7BAE-6589-4570-B6A3-3E28889392E7}</Property>
				<Property Name="DistPart[2].SoftDepCount" Type="Int">13</Property>
				<Property Name="DistPart[2].upgradeCode" Type="Str">{620DBAE1-B159-4204-8186-0813C8A6434C}</Property>
				<Property Name="DistPartCount" Type="Int">3</Property>
				<Property Name="INST_author" Type="Str">Nesto Research &amp; Development</Property>
				<Property Name="INST_autoIncrement" Type="Bool">true</Property>
				<Property Name="INST_buildLocation" Type="Path">../INSTALL</Property>
				<Property Name="INST_buildLocation.type" Type="Str">relativeToCommon</Property>
				<Property Name="INST_buildSpecName" Type="Str">RSAT Acquire Installer</Property>
				<Property Name="INST_defaultDir" Type="Str">{3833823B-68A5-4F98-A59C-7C63B22057CF}</Property>
				<Property Name="INST_productName" Type="Str">RSAT Acquire</Property>
				<Property Name="INST_productVersion" Type="Str">1.0.6</Property>
				<Property Name="InstSpecBitness" Type="Str">32-bit</Property>
				<Property Name="InstSpecVersion" Type="Str">17008005</Property>
				<Property Name="MSI_arpCompany" Type="Str">Somatidio / Nesto Research &amp; Development</Property>
				<Property Name="MSI_arpContact" Type="Str">info@nesto.nl</Property>
				<Property Name="MSI_arpURL" Type="Str">www.nest0.nl</Property>
				<Property Name="MSI_distID" Type="Str">{E70071FC-1439-432C-AD4E-D5E3F30EA5BE}</Property>
				<Property Name="MSI_hideNonRuntimes" Type="Bool">true</Property>
				<Property Name="MSI_osCheck" Type="Int">0</Property>
				<Property Name="MSI_upgradeCode" Type="Str">{975FA04F-A455-4E11-A1B3-35B58E224B94}</Property>
				<Property Name="RegDest[0].dirName" Type="Str">Software</Property>
				<Property Name="RegDest[0].dirTag" Type="Str">{DDFAFC8B-E728-4AC8-96DE-B920EBB97A86}</Property>
				<Property Name="RegDest[0].parentTag" Type="Str">2</Property>
				<Property Name="RegDestCount" Type="Int">1</Property>
				<Property Name="Source[0].dest" Type="Str">{3833823B-68A5-4F98-A59C-7C63B22057CF}</Property>
				<Property Name="Source[0].File[0].dest" Type="Str">{3833823B-68A5-4F98-A59C-7C63B22057CF}</Property>
				<Property Name="Source[0].File[0].name" Type="Str">RSAT Acquire.exe</Property>
				<Property Name="Source[0].File[0].Shortcut[0].destIndex" Type="Int">0</Property>
				<Property Name="Source[0].File[0].Shortcut[0].name" Type="Str">RSAT Acquire</Property>
				<Property Name="Source[0].File[0].Shortcut[0].subDir" Type="Str">RSAT</Property>
				<Property Name="Source[0].File[0].Shortcut[1].destIndex" Type="Int">1</Property>
				<Property Name="Source[0].File[0].Shortcut[1].name" Type="Str">RSAT Acquire</Property>
				<Property Name="Source[0].File[0].Shortcut[1].subDir" Type="Str">RSAT</Property>
				<Property Name="Source[0].File[0].ShortcutCount" Type="Int">2</Property>
				<Property Name="Source[0].File[0].tag" Type="Str">{0B660A33-4888-40EC-A72F-8E81FA66BA6C}</Property>
				<Property Name="Source[0].FileCount" Type="Int">1</Property>
				<Property Name="Source[0].name" Type="Str">RSAT Acquire</Property>
				<Property Name="Source[0].tag" Type="Ref">/My Computer/Build Specifications/RSAT Acquire</Property>
				<Property Name="Source[0].type" Type="Str">EXE</Property>
				<Property Name="SourceCount" Type="Int">1</Property>
			</Item>
		</Item>
	</Item>
</Project>
